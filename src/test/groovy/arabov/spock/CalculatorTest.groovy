package arabov.spock

import spock.lang.Specification
import spock.lang.Unroll

import static arabov.spock.Calculator.take

@Unroll
class CalculatorTest extends Specification {

    def "Должен складывать числа: #x + #y = #sum"() {

        expect:
            take(x).add(y).calculate() == sum

        where:
            x | y  | sum
            1 | 2  | 3
            2 | -5  | -3
            3 | -1 | 2
    }

    def "Должен вычитать числа"() {

        given:
            Calculator calculator = new Calculator()
        when:
            calculator.add(4).subtract(4)
        then:
            calculator.calculate() == 0
    }

    def "Должен умножать числа"() {

        expect:
            take(2).multiply(2).calculate() == 4
    }

    def "Должен вычислять сложную функцию"() {

        expect:
            take(7).multiply(2).subtract(4).multiply(2).calculate() == 20
    }

}
